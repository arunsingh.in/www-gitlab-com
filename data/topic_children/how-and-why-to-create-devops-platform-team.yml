title: What a DevOps platform team can do for your organization
description: "If your DIY DevOps effort is overwhelmed by infrastructure support
  needs, it’s time to consider a cutting-edge addition: a DevOps platform team."
header_body: "If your DIY DevOps effort is overwhelmed by infrastructure support
  needs, it's time to consider a cutting-edge addition: a DevOps platform team."
canonical_path: /topics/devops/how-and-why-to-create-devops-platform-team/
file_name: how-and-why-to-create-DevOps-platform-team
parent_topic: devops
twitter_image: /images/opengraph/gitlab-blog-cover.png
body: >-
  Adopting a DevOps platform won't just improve cycle times – it also provides
  an opportunity to rethink traditional roles, particularly on the ops side. Our
  [2022 Global DevSecOps Survey](https://about.gitlab.com/developer-survey/)
  shows that all DevOps roles are changing, but that was especially true in
  operations. Ops pros told us they were taking on new responsibilities
  including managing the cloud, maintaining the toolchain, DevOps coaching,
  automation, and platform engineering. Some organizations are going further and
  creating a DevOps platform team to help with the unique challenges of advanced
  DevOps.


  Here’s a look at why, and how one could fit into a [DevOps](/topics/devops/) organization.


  ## Start with the DevOps platform


  Less is certainly more when it comes to a [DevOps platform](https://about.gitlab.com/solutions/devops-platform/); it brings all the steps necessary to develop, secure, and release software together in one place and ends the toolchain “tax.” The platform can serve up advanced technologies from Kubernetes to [microservices](https://about.gitlab.com/topics/microservices/) and infrastructure as code (IaC), and as such, it needs an owner. In the past, a site reliability engineer (SRE) might have been tasked with some of those responsibilities, but today some organizations are looking to hire DevOps platform engineers in order to create a DevOps platform team.
   
  Not every company with a DevOps platform will need a team, however. An organization without legacy systems might not need this level of focus on infrastructure, while one with both cloud environments and on-premises data centers will likely want extra help supporting all the moving parts.


  ## What a DevOps platform team can do


  At its heart, a DevOps platform team will free their internal customers — ops (and devs for that matter) — from the complex and sometimes messy struggle to support the infrastructure. The goal is, of course, to offer as much self-service as possible for dev and ops, which means a streamlined process and less touchy experience. A DevOps platform team can “tame the beast,” making it possible for devs to do push-button deployments without any extra involvement.


  A DevOps platform team will likely take full advantage of infrastructure as code so manual interventions aren't required. Devs will benefit from an API-interface that will allow them to do their jobs without actually having to understand how the infrastructure is created.


  For some organizations, a DevOps platform team is a way to [maximize engineering efficiency](https://hackernoon.com/how-to-build-a-platform-team-now-the-secrets-to-successul-engineering-8a9b6a4d2c8), and for others it allows for a focus on best practices, an end to ad-hoc platform “volunteer managers” who won't have a broad view of business goals, and an increase in business agility.


  ## It’s not platform engineer vs. DevOps


  A platform engineering team is an extension of a DevOps team, not a replacement for it. Some practitioners warn of the risks of accidentally creating a secondary DevOps team while trying to create a platform team.


  Also, it's important to keep in mind that [platform engineers](https://about.gitlab.com/topics/devops/what-is-a-devops-platform-engineer/) need a broad set of skills ranging from security to Linux to Kubernetes, not to mention soft skills like communication and collaboration. They should be laser focused on infrastructure, not product development.


  ## What's different about platform teams?


  A platform engineering team is a central team focused on building, maintaining, and optimizing the core systems — the DevOps platform or other software development tools and cloud infrastructure, in particular — that enable other teams to ship high quality software securely and regularly to the organization's external customers. The platform team's customers are internal and often include development, operations, product management, and product teams.


  Here are some of the things they may be working on at any given time:

  * Finding new solutions to modernize or replace old systems and legacy tools, and helping other teams migrate to them

  * Making different cloud service providers work together better

  * Building a platform that helps the organization more fully adopt practices like continuous integration and continuous delivery (CI/CD) and automated testing

  * Working on ways to continuously improve the stability, efficiency and performance of the platform
resources_title: Read more about DevOps
resources:
  - title: The Benefits of DevOps
    url: https://www.youtube.com/watch?v=MNxkyLrA5Aw&feature=emb_logo
    type: Video
  - url: https://about.gitlab.com/customers/axway-devops/
    title: Axway aims for elite DevOps status
    type: Case studies
suggested_content:
  - url: /blog/2020/10/30/future-proof-your-developer-career/
  - url: /blog/2019/10/07/auto-devops-explained/
  - url: /blog/2018/01/22/a-beginners-guide-to-continuous-integration
